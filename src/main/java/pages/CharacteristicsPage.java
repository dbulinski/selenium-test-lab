package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class CharacteristicsPage extends HomePage {

    public CharacteristicsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".title_left>h3")
    private WebElement charHEADER;

    //    CHARACTERISTICS
    public CharacteristicsPage characteristicsHEADER() {
        Assert.assertTrue(charHEADER.isDisplayed(), "HEADER is not shown");
        Assert.assertTrue(charHEADER.getText().contains("Processes"));
        return this;
    }

    public CharacteristicsPage assertCharacteristicsUrl(String s) {
        String url = driver.getCurrentUrl();
        Assert.assertEquals(url, "http://localhost:4444/Characteristics");
        return this;
    }
}
