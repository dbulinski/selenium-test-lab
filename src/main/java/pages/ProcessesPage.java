package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class ProcessesPage extends HomePage {

    public ProcessesPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".title_left>h3")
    private WebElement processesHEADER;

    //    PROCESSES
    public ProcessesPage processesHEADER() {
        Assert.assertTrue(processesHEADER.isDisplayed(), "HEADER is not shown");
        Assert.assertTrue(processesHEADER.getText().contains("Processes"));
        return this;
    }

    public ProcessesPage assertProcessesUrl(String s) {
        String url = driver.getCurrentUrl();
        Assert.assertEquals(url, "http://localhost:4444/Projects");
        return this;
    }

}

