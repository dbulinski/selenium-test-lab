package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class DashboardPage extends HomePage {

    public DashboardPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".title_left>h3")
    private WebElement dashHeader;

    //    DASHBOARD
    public DashboardPage dashboardHEADER() {
        Assert.assertTrue(dashHeader.isDisplayed(), "HEADER is not shown");
        Assert.assertTrue(dashHeader.getText().contains("Processes"));
        return this;
    }

    public DashboardPage assertDashboardUrl(String s) {
        String url = driver.getCurrentUrl();
        Assert.assertEquals(url, "http://localhost:4444/");
        return this;
    }
}
//ogarnąć do CHARACTERSTICS  I PROCESS JAK TUTAJ^