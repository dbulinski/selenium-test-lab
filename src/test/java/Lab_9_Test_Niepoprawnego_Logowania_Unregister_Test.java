import org.testng.annotations.Test;
import pages.LoginPage;

public class Lab_9_Test_Niepoprawnego_Logowania_Unregister_Test extends SeleniumBaseTest {

    @Test
    public void incorecctLogingTestUnregisterWithChaining() {
        new LoginPage(driver)
                .typeEmail("test2@test.com")
                .typePassword("Test1!")
                .submitLoginwithFailure()
                .assertLoginIsShown("Invalid login attempt.");
    }

}
