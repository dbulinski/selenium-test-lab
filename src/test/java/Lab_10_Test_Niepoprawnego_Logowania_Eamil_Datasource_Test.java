import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;

public class Lab_10_Test_Niepoprawnego_Logowania_Eamil_Datasource_Test extends SeleniumBaseTest {


    @DataProvider
    public Object[][] getWrongEmails() {
        return new Object[][]{
                {"test", "The Email field is not a valid e-mail address."},
                {"admin", "The Email field is not a valid e-mail address."},
                {"@test", "The Email field is not a valid e-mail address."}
        };
    }

    @Test(dataProvider = "getWrongEmails")
    public void incorrectEmailTestWithChaining(String wrongEmail, String expectedEmail) {
        new LoginPage(driver)
                .typeEmail(wrongEmail)
                .typePassword("")
                .submitLoginwithFailure()
                .assertLoginIsShown("The Email field is not a valid e-mail address.");
    }
}
