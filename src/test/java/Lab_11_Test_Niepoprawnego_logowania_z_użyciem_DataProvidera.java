import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.CreateAccountPage;
import pages.LoginPage;

public class Lab_11_Test_Niepoprawnego_logowania_z_użyciem_DataProvidera extends SeleniumBaseTest {

    @DataProvider
    public Object[][] getWrongEmails() {
        return new Object[][]{
                {"test",},
                {"admin"},
                {"@test"}
        };
    }

    @Test(dataProvider = "getWrongEmails")
    public void incorrectLoginDP(String wrongEmail) {
        LoginPage loginPage = new LoginPage(driver);
        CreateAccountPage createAccountPage = loginPage.goToRegisterPage();
        createAccountPage.typeEmail(wrongEmail);
        createAccountPage.typePassword("");
        createAccountPage.registerWithFailure();
        createAccountPage.assertErrorIsShown("The Email field is not a valid e-mail address.");
    }

}
