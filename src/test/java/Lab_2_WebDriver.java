import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import java.time.Duration;

public class Lab_2_WebDriver {
    @Test
    public void playWithWebDriver() {
        System.setProperty("webdriver.chrome.driver", "c:/dev/driver/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.get("https://www.google.com");
        driver.manage().window().fullscreen();


        WebElement agreeBtn = driver.findElement(By.cssSelector("#L2AGLb"));
        agreeBtn.click();

        WebElement searchTxt = driver.findElement(By.cssSelector("input[name=q]"));
        searchTxt.sendKeys("QA");

        WebElement searchBtn = driver.findElement(By.cssSelector("input[value='Szukaj w Google'"));
        searchBtn.click();
        driver.manage().window().fullscreen();
        driver.quit();



    }
}