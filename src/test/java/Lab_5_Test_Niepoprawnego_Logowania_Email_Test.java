import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.Duration;

public class Lab_5_Test_Niepoprawnego_Logowania_Email_Test {

    @Test
    public void incorecctLogingTestWrongEmail (){
        System.setProperty("webdriver.chrome.driver", "c:/dev/driver/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.manage().window().maximize();
        driver.get("http://localhost:4444/");

        //web elementy
        WebElement emialTxt = driver.findElement(By.cssSelector("#Email"));
        emialTxt.sendKeys("example");

        WebElement passwordTxt = driver.findElement(By.cssSelector("#Password"));
        passwordTxt.sendKeys("Test1!");

        WebElement loginBtn = driver.findElement(By.cssSelector("button[type=submit]"));
        loginBtn.click();

        // rozwiązanie nr.1
        WebElement emailError = driver.findElement(By.cssSelector("#Email-error"));
        Assert.assertEquals(emailError.getText(), "The Email field is not a valid e-mail address.");

        driver.quit();

    }
}
