
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;


public class Lab_8_Test_Poprawnego_Logowania_POP_Test extends SeleniumBaseTest {

    @Test
    public void correctLogintest() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.typeEmail("test@test.com");
        loginPage.typePassword("Test1!");

        HomePage homePage = loginPage.submitLogin();
        homePage.assertWelcomeElementIsShown();
    }
    @Test
    public void correctLoginTestWithChaining () {
        new LoginPage(driver)
                .typeEmail("test@test.com")
                .typePassword("Test1!")
                .submitLogin()
                .assertWelcomeElementIsShown();
    }


}
