import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.List;

public class Lab_6_Test_Niepoprawnego_Logowania_Hasło_Test {
    @Test
    public void incorecctPasswordTestWrongEmail() {
        System.setProperty("webdriver.chrome.driver", "c:/dev/driver/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.manage().window().maximize();
        driver.get("http://localhost:4444/");

        //web elementy
        WebElement emialTxt = driver.findElement(By.cssSelector("#Email"));
        emialTxt.sendKeys("test@test.com");

        WebElement passwordTxt = driver.findElement(By.cssSelector("#Password"));
        passwordTxt.sendKeys("examplePassword");

        WebElement loginBtn = driver.findElement(By.cssSelector("button[type=submit]"));
        loginBtn.click();

        //WALIDACJA
        List<WebElement> validationErrors = driver.findElements(By.cssSelector(".validation-summary-error>ul>li"));

        boolean doesErrorExists = false;
        for (int i = 0; i<validationErrors.size (); i++){
            if (validationErrors.get(i).getText().equals("Invalid login attempt.")){
                doesErrorExists = true;
                break;
            }
        }
        driver.quit();

    }
}